<?php

class Desicardboard extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function pre($data) {
        echo '<pre>';
        print_r($data);
        echo "</pre>";
        exit;
    }

    function index() {
        $this->title = "Login";
        $this->load->view("products");
    }

    function invite() {
        $this->load->library("form_validation");

        $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
        $this->form_validation->set_rules("message", "Message", "trim|required");

        if ($this->form_validation->run() == false) {
            $this->load->view("invite");
        } else {

            $res = $this->db->get_where("users", array("email" => $this->input->post("email")));
            $message = $this->input->post("message");
            $str = $this->generateRandomString();
            if ($res->num_rows()) {
                $inv_id = $res->row()->id;
            } else {
                $data['password'] = sha1($str);
                $data['email'] = $this->input->post("email");
                $data['date_added'] = date("Y-m-d H:i:s");
                $data['date_modified'] = date("Y-m-d H:i:s");
                $this->db->insert("users", $data);
                $inv_id = $this->db->insert_id();
                $message = $this->input->post("message") . " and your password is " . $str;
            }

            if ($inv_id == $this->session->userdata("id")) {
                $this->session->set_flashdata("error", "You cannot invite yourself.");
            } else {

                $res1 = $this->db->get_where("orders", array("userid" => $this->session->userdata("id")));
                if (1) {

                    $a = $this->send_email($this->input->post("email"), $message, "Invitation");
                    if ($a) {
                        $this->session->set_flashdata("success", "Invitation Sent Successfully.");
                        $in['invite_id'] = $inv_id;
                        $in['user_id'] = $this->session->userdata("id");
                        $in['date_added'] = date("Y-m-d H:i:s");

                        $this->db->insert("invites", $in);
                    } else
                        $this->session->set_flashdata("error", "Invitation Not Sent, Try again later.");
                } else {
                    $this->session->set_flashdata("error", "You should purchase the product to invite your friends.");
                }
            }

            redirect("invite-friends");
        }
    }

    function change_password() {


        $this->load->database();
        $this->load->library("form_validation");

        $this->form_validation->set_rules("pass", "Password", "trim|required|matches[conf]|min_length[6]");
        $this->form_validation->set_rules("conf", "Confirm Password", "trim|required|matches[pass]|min_length[6]");

        if ($this->form_validation->run() == false) {
            $this->load->view("change-password");
        } else {
            $data['password'] = sha1($this->input->post("pass"));
            $data['date_modified'] = date("Y-m-d H:i:s");
            $this->db->where("email", $this->session->userdata("email"));
            $this->db->where("id", $this->session->userdata("id"));
            $this->db->update("users", $data);

            $this->session->set_flashdata("success", "Password Changed Successfully.");
            redirect("change-password");
        }
    }

    function track_invitation() {
        $this->db->select("users.id,invites.date_added,users.email");
        $this->db->join("users", "users.id = invites.invite_id");
        $this->db->group_by("users.id");
        $data['res'] = $this->db->get_where("invites", array("user_id" => $this->session->userdata("id")))->result();
        $i = 0;
        foreach ($data['res'] as $a) {
            $data['res'][$i]->purchased = $this->get_count($a->id);
            $i++;
        }
//        $this->pre($data);
        $this->load->view("track-invitation", $data);
    }

    function get_count($id) {
        return $this->db->get_where("orders", array("userid" => $id))->num_rows();
    }

    function forgot_password() {
        $this->load->library("form_validation");
        $this->form_validation->set_message("email", "Email is not registerd with us.");
        $this->form_validation->set_rules("email", "Email", "required|trim|valid_email");

        if ($this->form_validation->run() == false) {
            $this->load->view("forgot-password");
        } else {
            if ($this->input->post("email")) {
                $email = $this->input->post("email");
                $email = base64_encode($email);
                $link = base_url("reset-password?email=$email");
                $message = "Hello,<br>
<br>
You recently requested to reset the password for your DesiCardboard account. <br>
<br>
Please click on the following URL to reset the password.<br>
$link<br>
<br>
If you did not send this request, you can ignore this mail.<br>
<br>
Warm Regards,<br>
The DesiCardboard Team.<br>
Web: desicardboard.com<br>
Email: support@desicardboard.com<br>
Phone: 09538111016";
                $res = $this->db->get_where("users", array("email" => $this->input->post("email")))->num_rows();
                if ($res) {
                    $this->send_email($this->input->post("email"), $message, "Your Password Reset Link");
                    $this->session->set_flashdata("success", "Please check your email for your password.");
                } else {
                    $this->session->set_flashdata("error", "Email is not registerd with us.");
                }
            } else {
                $this->session->set_flashdata("error", "Email is not registerd with us.");
            }

            redirect("forgot-password");
        }
    }

    function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function reset_password() {
        if (array_key_exists("email", $_GET)) {
            $email = base64_decode($_GET['email']);
            $this->session->set_userdata("reset_email", $email);
            $res = $this->db->get_where("users", array("email" => $email))->num_rows();
            if ($res) {
                $this->load->view("reset-password");
            } else {
                $this->load->view("error");
            }
        } else {
            if ($this->input->post(null, false)) {
                $this->load->library("form_validation");

                $this->form_validation->set_rules("pass", "Password", "trim|required|matches[conf]|min_length[6]");
                $this->form_validation->set_rules("conf", "Confirm Password", "trim|required|matches[pass]|min_length[6]");

                if ($this->form_validation->run() == false) {
                    $this->load->view("reset-password");
                } else {
                    $data['password'] = sha1($this->input->post("pass"));
                    $data['date_modified'] = date("Y-m-d H:i:s");
                    $this->db->where("email", $this->session->userdata("reset_email"));
                    $this->db->update("users", $data);

                    $this->session->set_flashdata("success", "Password Changed Successfully.");
                    redirect("login");
                }
            } else {
                $this->load->view("error");
            }
        }
    }

    function send_email($to, $message, $sub) {
        $this->load->library('email');
        $config = array();
        $config = $this->mail_config();
        $config['mailtype'] = "html";
        $this->email->initialize($config);
        $this->email->from('support@desicardboard.com', 'Desicardboard');
        $this->email->to($to);
        $this->email->subject($sub);
        $this->email->message($message);
        return $this->email->send();
//        echo ($this->email->send());
//        exit;
    }

    function mail_config() {
        $config = array();
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "smtp.mandrillapp.com";
        $config['smtp_user'] = "desicardboard@gmail.com";
        $config['smtp_pass'] = "5gqCwxtUpJwqsZshctp-tg";
        $config['smtp_port'] = "587";
        return $config;
    }

}
