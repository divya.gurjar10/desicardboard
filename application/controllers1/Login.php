<?php

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->no_cache();
        $this->load->model('loginmodel');
    }

    function index() {
        if ($this->input->post()) {
            
            $email = $this->input->post('email');
            $pass = $this->input->post('password');
            $a = $this->loginmodel->check_access($email, $pass);
            if ($a) {
                redirect('desicardboard');
            } else {
                $this->session->set_flashdata('error', 'Authentication failed');
                redirect('login');
            }
        }
        $this->load->view('login');
    }

    function forgot_password() {
        $data['error'] = "";
        $data['success'] = "";

        if ($this->input->post(NULL, TRUE)) {
            require_once('recaptchalib.php');
            $privatekey = "6Lfjv-0SAAAAALKOfIE1pePu68ucEqeBXF8UoAHx";
            $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]
            );

            if (!$resp->is_valid) {
                //$data['reset'] = FALSE;
                //When the CAPTCHA was entered incorrectly
                $data['error'] = "The reCAPTCHA wasn't entered correctly.";
            } else {
                $username = $this->input->post('username');

                $this->db->where('email', $username);
                $result = $this->db->get('admin');

                if ($result->num_rows > 0) {
                    $validated = $result->row();

                    $new_pwd = $this->generateNewString();
                    //echo $new_pwd;
                    $dt = date('Y-m-d H:i:s');
                    $data = array(
                        'password' => sha1($new_pwd),
                        'last_updated' => $dt,
                    );
                    $this->db->where('email', $username);
                    $this->db->update('admin', $data);

                    $message = "<p><span style='font-size: 12pt;'>Please find below your login details as requested:</span></p>";
                    $message .= "<p><span style='font-size: 12pt;'>USERNAME:&nbsp;" . $username . "</span></p>";
                    $message .= "<p><span style='font-size: 12pt;'>PASSWORD:&nbsp;" . $new_pwd . "</span></p>";
                    $message .= "<p><span style='font-size: 12pt;'>&nbsp;</span></p>";

                    //Loading email library
                    $this->load->library('email');

                    $config['mailtype'] = 'html';

                    $this->email->initialize($config);

                    $this->email->from('no-reply@daraloud.com');
                    $this->email->to($username);

                    $this->email->subject('DAO World Admin Forgot Password');

                    $this->email->message($message);

                    if ($this->email->send()) {
                        $data['success'] = "Your password is successfully reset and send to your regestired email id.";
                    } else {
                        $data['error'] = $this->email->print_debugger();
                    }
                } else {
                    $data['error'] = "This email-id is not registered.";
                }
            }
        }
        $this->load->view($this->config->item('admin_folder') . '/forgot_password', $data);
    }

    function generateNewString() {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < 6; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    function logout() {
        $this->session->unset_userdata('name');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('student_loggedin');
//        $this->session->sess_destroy();
        $this->session->set_flashdata('success', "Logged out successfully!!!.");
        redirect('login/');
    }

    function pre($data) {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        exit;
    }

    private function no_cache() {
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i(worry)") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    }

    function check_email() {
        $email = $this->input->post('email');
        $this->db->where('email', $email);
        $res = $this->db->get('students');
        if ($res->num_rows) {
            $new_pwd = $this->generateNewString();
            $message = "<p><span style='font-size: 12pt;'>Please find below your login details as requested:</span></p>";
            $message .= "<p><span style='font-size: 12pt;'>USERNAME:&nbsp;" . $email . "</span></p>";
            $message .= "<p><span style='font-size: 12pt;'>PASSWORD:&nbsp;" . $new_pwd . "</span></p>";
            $message .= "<p><span style='font-size: 12pt;'>&nbsp;</span></p>";
            $this->load->library('email');
            $config = array();
            $config = $this->mail_config();
            $config['mailtype'] = "html";
            $this->email->initialize($config);
            $this->email->from('no-reply@keet.co.in');
            $this->email->to("niranjan.raju007@gmail.com");
            $this->email->subject("Forgot Password - Keet");
            $this->email->message($message);
            if($this->email->send())
                echo "success";
        } else {
            echo "error";
        }
    }

    function mail_config() {
        $config = array();
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "smtp.mandrillapp.com";
        $config['smtp_user'] = "niranjan.raju007@gmail.com";
        $config['smtp_pass'] = "DkT3-iNNdNs6pwE6SP342w";
        $config['smtp_port'] = "587";
        return $config;
    }

}
