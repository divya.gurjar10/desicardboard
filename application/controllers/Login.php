<?php

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->no_cache();
        $this->load->model('loginmodel');
    }

    function index() 
    {
        if ($this->input->post()) 
        {
            $email = $this->input->post('email');
            $pass = $this->input->post('password');
            $a = $this->loginmodel->check_access($email, $pass);

            if ($a) 
            {
                if ($a->activated == 1) 
                {
                    redirect('desicardboard');
                } 
                else 
                {
                    $this->session->set_flashdata('error', 'Check you email for activation link, and activate your account');
                    redirect('login');
                }
            } 
            else 
            {
                $this->session->set_flashdata('error', 'Authentication failed');
                redirect('login');
            }
        }
        $this->load->view('login');
    }

    function forgot_password() {
        $this->load->library("form_validation");
        $this->form_validation->set_message("email", "Email is not registerd with us.");
        $this->form_validation->set_rules("email", "Email", "required|trim|valid_email");

        if ($this->form_validation->run() == false) {
            $this->load->view("forgot-password");
        } else {
            if ($this->input->post("email")) {
             $email = $this->input->post("email");
             $email = base64_encode($email);
             $link = base_url("reset-password?email=$email");
             $message = "Hello,<br>
             <br>
             You recently requested to reset the password for your DesiCardboard account. <br>
             <br>
             Please click on the following URL to reset the password.<br>
             <a href='$link'>Click Here To Reset Your Password</a><br>
             <br>
             If you did not send this request, you can ignore this mail.<br>
             <br>
             Warm Regards,<br>
             The DesiCardboard Team.<br>
             Web: desicardboard.com<br>
             Email: support@desicardboard.com<br>
             ";
             $res = $this->db->get_where("users", array("email" => $this->input->post("email")))->num_rows();
             if ($res) {
                $email = $this->input->post("email");
                
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type:text/html;charset=iso-8859-1' . "\r\n";
                $headers .= 'From: <no-replay@desicardboard.com>' . "\r\n";
                mail($this->input->post("email"),"Forgot Password",$message,$headers);

                $this->session->set_flashdata("success", "Please check your email for your password.");
            } else {
                $this->session->set_flashdata("error", "Email is not registerd with us.");
            }
        } else {
            $this->session->set_flashdata("error", "Email is not registerd with us.");
        }

        redirect("forgot-password");
    }
}

function generateRandomString($length = 6) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function reset_password() {

    if (array_key_exists("email", $_GET)) {
        $email = base64_decode($_GET['email']);
        $this->session->set_userdata("reset_email", $email);
        $res = $this->db->get_where("users", array("email" => $email))->num_rows();
        if ($res) {
            $this->load->view("reset-password");
        } else {
            $this->load->view("error");
        }
    } else {
        if ($this->input->post(null, false)) {
            $this->load->library("form_validation");

            $this->form_validation->set_rules("pass", "Password", "trim|required|matches[conf]|min_length[6]");
            $this->form_validation->set_rules("conf", "Confirm Password", "trim|required|matches[pass]|min_length[6]");

            if ($this->form_validation->run() == false) {
                $this->load->view("reset-password");
            } else {
                $data['password'] = sha1($this->input->post("pass"));
                $data['date_modified'] = date("Y-m-d H:i:s");
                $this->db->where("email", $this->session->userdata("reset_email"));
                $this->db->update("users", $data);

                $this->session->set_flashdata("success", "Password Changed Successfully.");
                redirect("login");
            }
        } else {
            $this->load->view("error");
        }
    }
}

function logout() {
    $this->session->unset_userdata('name');
    $this->session->unset_userdata('email');
    $this->session->unset_userdata('id');
    $this->session->unset_userdata('student_loggedin');
//        $this->session->sess_destroy();
    $this->session->set_flashdata('success', "Logged out successfully!!!.");
    redirect('login/');
}

function pre($data) {
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    exit;
}

private function no_cache() {
    $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i(worry)") . ' GMT');
    $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
    $this->output->set_header('Pragma: no-cache');
    $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
}

function check_email() {
    $email = $this->input->post('email');
    $this->db->where('email', $email);
    $res = $this->db->get('students');
    if ($res->num_rows) {
        $new_pwd = $this->generateNewString();
        $message = "<p><span style='font-size: 12pt;'>Please find below your login details as requested:</span></p>";
        $message .= "<p><span style='font-size: 12pt;'>USERNAME:&nbsp;" . $email . "</span></p>";
        $message .= "<p><span style='font-size: 12pt;'>PASSWORD:&nbsp;" . $new_pwd . "</span></p>";
        $message .= "<p><span style='font-size: 12pt;'>&nbsp;</span></p>";
        $this->load->library('email');
        $config = array();
        $config = $this->mail_config();
        $config['mailtype'] = "html";
        $this->email->initialize($config);
        $this->email->from('no-reply@keet.co.in');
        $this->email->to("niranjan.raju007@gmail.com");
        $this->email->subject("Forgot Password - Keet");
        $this->email->message($message);
        if ($this->email->send())
            echo "success";
    } else {
        echo "error";
    }
}

function verify_email() {

    $email = base64_decode($_GET['e']);

    $res = $this->db->get_where("users",array("email" => $email));
    if($res->num_rows()){
        $res = $res->row();
        if($res->activated == 0)
        {
            $data['activated'] = 1;
            $this->db->where("email",$email);
            $this->db->update("users",$data);
        } 
        $this->session->set_flashdata("success", "Your account has been activated, Please login and enjoy our product.");
        redirect("login");
    } else {
        $this->session->set_flashdata("error", "Some error occurred. Please contact Admin.");
        redirect("login");
    }
}

function mail_config() {
    $config = array();
    $config['protocol'] = "smtp";
    $config['smtp_host'] = "smtp.mandrillapp.com";
    $config['smtp_user'] = "desicardboard@gmail.com";
    $config['smtp_pass'] = "5gqCwxtUpJwqsZshctp-tg";
    $config['smtp_port'] = "587";
    return $config;
}

function send_email($to, $message, $sub) {
    $this->load->library('email');
    $config = array();
    $config = $this->mail_config();
    $config['mailtype'] = "html";
    $this->email->initialize($config);
    $this->email->from('support@desicardboard.com', 'Desicardboard');
    $this->email->to($to);
    $this->email->subject($sub);
    $this->email->message($message);
    return $this->email->send();
//        echo ($this->email->send());
//        exit;
}

}
