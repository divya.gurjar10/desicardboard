<?php

class Desicardboard extends Admin_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->no_cache();
    }
    
    
    function index()
    {
        $this->title = "Login";
        
        $query = "Select * from orders where DATE(date) = '" . date("Y-m-d") . "'";
        
        $res = $this->db->query($query);
        
        if ($res->num_rows() <= 200) {
            $data['available'] = 1;
        } else {
            $data['available'] = 0;
        }
        $data['purchased'] = $this->db->get_where("orders", array(
            "user_id" => $this->session->userdata("id")
        ))->num_rows();
        $this->db->select("brand");
        $this->db->group_by("brand");
        $res = $this->db->get("compatability")->result();
        foreach ($res as $row) {
            $data['brand'][] = $row->brand;
        }
        $data['pro_count'] = $this->db->get_where("orders", array(
            "DATE(date)" => date("Y-m-d")
        ))->num_rows();
        
        $this->load->view("products", $data);
    }
    
    
    function invite()
    {
        $this->load->library("form_validation");
        
        $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
        $this->form_validation->set_rules("name", "Name", "trim|required");
        
        if ($this->form_validation->run() == false) {
            $this->load->view("invite");
        } else {
            
            $res  = $this->db->get_where("users", array(
                "email" => $this->input->post("email")
            ));
            $a    = base64_encode($this->input->post("email"));
            $link = base_url("verify-account?e=$a");
            
            $str = $this->generateRandomString();
            if ($res->num_rows()) {
                
                $inv_id = $res->row()->id;
            } else {
                $data['password']      = sha1($str);
                $data['email']         = $this->input->post("email");
                $data['name']          = $this->input->post("name");
                $data['date_added']    = date("Y-m-d H:i:s");
                $data['date_modified'] = date("Y-m-d H:i:s");
                $this->db->insert("users", $data);
                $inv_id = $this->db->insert_id();
                ;
            }
            
            $num = $this->db->get_where("invites", array(
                "user_id" => $this->session->userdata("id")
            ));
            if ($this->session->userdata("id") == 1) {
                $total = 1;
            } else {
                $total = $num->num_rows();
            }
            if ($total <= 9) {
                $res1 = $this->db->get_where("orders", array(
                    "user_id" => $this->session->userdata("id")
                ));
                if ($res1->num_rows()) { // $res1->num_rows()
                    if (!$res->num_rows()) {
                        $email   = $this->input->post("email");
                        $name    = $this->session->userdata("name");
                        $headers = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type:text/html;charset=iso-8859-1' . "\r\n";
                        $headers .= 'From: <no-replay@desicardboard.com>' . "\r\n";
                        $date    = date("jS M Y H:i A", strtotime("+2 day"));
                        $message = "Hello There, <br><br>
                            $name Just purchased a Desicardboard and wants you to check it out.<br>
                            Did you know that you can watch 3D movies, Take a roller coaster ride, Take a virtual tour to the grand<br>
                            canyon Just using your smartphone and a Desicardboard Virtual reality Kit being where you are? Isn’t <br>
                            that futuristic?<br><br>
                            Here’s a small video on Tedtalks about Virtual reality if you are wondering what Virtual reality is<br><br>
                            https://www.youtube.com/watch?v=yRUCU5FsvTY<br><br>
                            So done seeing the video? While the cost of experiencing Virtual reality technology is still over a <br>
                            $1000.00, We at Desicardboard want to make this accessible to a million Indians by the 2015, so we <br>
                            started this revolution.  Simply join the VR revolution by purchasing our Version 2.0 @ Just Rs. 444 and <br>
                            invite 3 friends to experience this technology and if you help 3 friends experience this technology. We <br>
                            would refund your INR 444 completely. <br><br>
                            Isn’t that Awesome? Click this <a href=" . $link . ">link</a> to join the VR revolution.<br><br>
                            Note: Your invite expires by $date So claim your offer and lets take this technology to every Desi <br>
                            out there.  <br><br>
                            Let your Screams come true!<br><br>

                            Login Credentials:<br>
                            Username : $email<br>
                            Password: $str<br>
                            <br>
                            Desi@Work<br>
                            www.desicardboard.com<br>
                            www.facebook.com/desicardboard<br>
                            www.twitter.com/desicardboard";
                        $sub     = "$name Invited you to get your free Desicardboard!!!";
                        $a       = mail($email, $sub, $message, $headers);
                        
                        if ($a) {
                            $this->session->set_flashdata("success", "Invitation Sent Successfully.");
                            $in['invite_id']  = $inv_id;
                            $in['user_id']    = $this->session->userdata("id");
                            $in['date_added'] = date("Y-m-d H:i:s");
                            
                            $this->db->insert("invites", $in);
                        } else
                            $this->session->set_flashdata("error", "Invitation Not Sent, Try again later.");
                    } else {
                        $this->session->set_flashdata("error", "An Invitation has already been sent to this user");
                    }
                } else {
                    $this->session->set_flashdata("error", "You should purchase the product to invite your friends.");
                }
                
            } else {
                $this->session->set_flashdata("error", "You can invite only 10 friends.");
            }
            
            redirect("invite-friends");
        }
    }
    
    function change_password()
    {
        
        
        $this->load->database();
        $this->load->library("form_validation");
        
        $this->form_validation->set_rules("pass", "Password", "trim|required|matches[conf]|min_length[6]");
        $this->form_validation->set_rules("conf", "Confirm Password", "trim|required|matches[pass]|min_length[6]");
        
        if ($this->form_validation->run() == false) {
            $this->load->view("change-password");
        } else {
            $data['password']      = sha1($this->input->post("pass"));
            $data['date_modified'] = date("Y-m-d H:i:s");
            $this->db->where("email", $this->session->userdata("email"));
            $this->db->where("id", $this->session->userdata("id"));
            $this->db->update("users", $data);
            
            $this->session->set_flashdata("success", "Password Changed Successfully.");
            redirect("change-password");
        }
    }
    
    function track_invitation()
    {
        $this->db->select("users.id,invites.date_added,users.email");
        $this->db->join("users", "users.id = invites.invite_id");
        $this->db->group_by("users.id");
        $data['res'] = $this->db->get_where("invites", array(
            "user_id" => $this->session->userdata("id")
        ))->result();
        $i           = 0;
        foreach ($data['res'] as $a) {
            $data['res'][$i]->purchased = $this->get_count($a->id);
            $i++;
        }
        //        $this->pre($data);
        $this->load->view("track-invitation", $data);
    }
    
    function get_count($id)
    {
        return $this->db->get_where("orders", array(
            "user_id" => $id
        ))->num_rows();
    }
    
    
    
    function ccavenue()
    {
        
        $res = $this->db->get_where("orders", array(
            "user_id" => $this->session->userdata("id")
        ));
        if ($res->num_rows()) {
            $this->session->set_flashdata("error", "You have already purchased the product.");
            redirect("desicardboard");
        } else
            $this->load->view("ccavenue");
    }
    
    function initialize()
    {
        $this->load->view("initialize");
    }
    
    function payment_success()
    {
        
        $workingKey    = '607FE23AC12F0ED57C84FA4132F137E7'; //Working Key should be provided here.
        $encResponse   = $_POST['encResp'];
        $rcvdString    = $this->decrypt($encResponse, $workingKey); //Crypto Decryption used as per the specified working key.
        $order_status  = "";
        $decryptValues = explode('&', $rcvdString);
        $dataSize      = sizeof($decryptValues);
        
        for ($i = 0; $i < $dataSize; $i++) {
            $information = explode('=', $decryptValues[$i]);
            if ($i == 3)
                $order_status = $information[1];
        }
        
        $data = array();
        if ($order_status === "Success") {
            $this->session->set_flashdata("success", "Payment Successful.");
            for ($i = 0; $i < $dataSize; $i++) {
                $information = explode('=', $decryptValues[$i]);
                if ($information[0] == "order_id" || $information[0] == "tracking_id" || $information[0] == "bank_ref_no" || $information[0] == "order_status" || $information[0] == "failure_message" || $information[0] == "payment_mode" || $information[0] == "card_name" || $information[0] == "status_code" || $information[0] == "status_message" || $information[0] == "amount" || $information[0] == "billing_name" || $information[0] == "billing_address" || $information[0] == "billing_city" || $information[0] == "billing_state" || $information[0] == "billing_zip" || $information[0] == "billing_country" || $information[0] == "billing_tel" || $information[0] == "billing_email" || $information[0] == "delivery_name" || $information[0] == "delivery_address" || $information[0] == "delivery_city" || $information[0] == "delivery_country" || $information[0] == "delivery_state" || $information[0] == "delivery_zip" || $information[0] == "delivery_tel")
                    $data[$information[0]] = $information[1];
            }
            $data['date']    = date("Y-m-d h:i:s");
            $data['user_id'] = $this->session->userdata("id");
            $this->db->insert("orders", $data);
            
            $msg     = $this->load->view("email-template", $data, true);
            //$this->send_email($this->session->userdata("email"), $msg, "Order Successfull");
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type:text/html;charset=iso-8859-1' . "\r\n";
            $headers .= 'From: <no-replay@desicardboard.com>' . "\r\n";
            mail($this->session->userdata("email"), "Order Successfull", $msg, $headers);
            
            
            $res = $this->db->get_where("invites", array(
                "invite_id" => $this->session->userdata("id")
            ));
            
            if ($res->num_rows()) {
                $res = $res->row();
                
                $this->db->join("orders", "orders.user_id = invites.invite_id");
                $res1 = $this->db->get_where("invites", array(
                    "invites.user_id" => $res->user_id
                ));
                
                if ($res1->num_rows() >= 3) {
                    $res2 = $this->db->get_where("users", array(
                        "id" => $res->user_id
                    ));
                    if ($res2->num_rows()) {
                        $res2    = $res2->row();
                        $message = "Refund amount to user $res2->name with email $res2->email";
                        mail("hello@desicardboard.com", "refund to customer", $message, $headers);
                    }
                }
            }
            $name  = $this->session->userdata("name");
            $email = $this->session->userdata("email");
            mail("hello@desicardboard.com", "Order Placed", "An order has been placed by user $name with email $email", $headers);
            
        } else if ($order_status === "Aborted") {
            $this->session->set_flashdata("error", "Payment Aborted, Try again after some time.");
        } else if ($order_status === "Failure") {
            $this->session->set_flashdata("error", "Payment Failed, Try again after some time.");
        } else {
            $this->session->set_flashdata("error", "Some error occurred, Try again after some time.");
        }
        
        redirect("desicardboard");
    }
    
    function payment_cancel()
    {
        $this->session->set_flashdata("error", "Payment Cancelled.");
        redirect("desicardboard");
    }
    
    
    
    function send_email($to, $message, $sub)
    {
        $this->load->library('email');
        $config             = array();
        $config             = $this->mail_config();
        $config['mailtype'] = "html";
        $this->email->initialize($config);
        $this->email->from('support@desicardboard.com', 'Desicardboard');
        $this->email->to($to);
        $this->email->subject($sub);
        $this->email->message($message);
        return $this->email->send();
        //        echo ($this->email->send());
        //        exit;
    }
    
    function encrypt($plainText, $key)
    {
        $secretKey  = $this->hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $openMode   = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
        $blockSize  = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
        $plainPad   = $this->pkcs5_pad($plainText, $blockSize);
        if (mcrypt_generic_init($openMode, $secretKey, $initVector) != -1) {
            $encryptedText = mcrypt_generic($openMode, $plainPad);
            mcrypt_generic_deinit($openMode);
        }
        return bin2hex($encryptedText);
    }
    
    function decrypt($encryptedText, $key)
    {
        $secretKey     = $this->hextobin(md5($key));
        $initVector    = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $encryptedText = $this->hextobin($encryptedText);
        $openMode      = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
        mcrypt_generic_init($openMode, $secretKey, $initVector);
        $decryptedText = mdecrypt_generic($openMode, $encryptedText);
        $decryptedText = rtrim($decryptedText, "\0");
        mcrypt_generic_deinit($openMode);
        return $decryptedText;
    }
    
    //*********** Padding Function *********************
    
    function pkcs5_pad($plainText, $blockSize)
    {
        $pad = $blockSize - (strlen($plainText) % $blockSize);
        return $plainText . str_repeat(chr($pad), $pad);
    }
    
    //********** Hexadecimal to Binary function for php 4.0 version ********
    
    function hextobin($hexString)
    {
        $length    = strlen($hexString);
        $binString = "";
        $count     = 0;
        while ($count < $length) {
            $subString    = substr($hexString, $count, 2);
            $packedString = pack("H*", $subString);
            if ($count == 0) {
                $binString = $packedString;
            } else {
                $binString .= $packedString;
            }
            
            $count += 2;
        }
        return $binString;
    }
    
    
    function generateRandomString($length = 6)
    {
        $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString     = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    function mail_config()
    {
        $config              = array();
        $config['protocol']  = "smtp";
        $config['smtp_host'] = "smtp.mandrillapp.com";
        $config['smtp_user'] = "desicardboard@gmail.com";
        $config['smtp_pass'] = "5gqCwxtUpJwqsZshctp-tg";
        $config['smtp_port'] = "587";
        return $config;
    }
    
    function get_model()
    {
        $a = "<option value='select'>Select Model</option>";
        if ($this->input->post("value")) {
            $res = $this->db->get_where("compatability", array(
                "brand" => $this->input->post("value")
            ));
            if ($res->num_rows()) {
                $res = $res->result();
                foreach ($res as $row) {
                    $a .= "<option value='$row->make'>" . $row->make . "</option>";
                }
            }
        }
        
        echo $a;
    }
    
    function get_text()
    {
        $a = "";
        if ($this->input->post("model") && $this->input->post("brand")) {
            $res = $this->db->get_where("compatability", array(
                "brand" => $this->input->post("brand"),
                "make" => $this->input->post("model")
            ));
            
            if ($res->num_rows()) {
                $res = $res->row();
                $a   = $res->message;
            }
        }
        
        echo $a;
    }
    
    function ord()
    {
        for ($i = 1; $i <= 2048; $i++) {
            $a[] = $i;
        }
        
        $a[] = 2084;
        
        foreach ($a as $row) {
            $data['user_id'] = $row;
            $this->db->insert("orders", $data);
        }
    }
    
}