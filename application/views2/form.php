<?php
        $Merchant_Id = "70121";
        $Amount = "1";
        $Order_Id = "100"; //unique Id that should be passed to payment gateway
        $WorkingKey = "607FE23AC12F0ED57C84FA4132F137E7"; //Given to merchant by ccavenue
        $Redirect_Url = "localhost/desicardbosrd/success-page";
        $Checksum = getCheckSum($Merchant_Id, $Amount, $Order_Id, $Redirect_Url, $WorkingKey);                 // Validate All value
//creating a signature using the given details for security reasons

        function getchecksum($MerchantId, $Amount, $OrderId, $URL, $WorkingKey) {
            $str = "$MerchantId|$OrderId|$Amount|$URL|$WorkingKey";
            $adler = 1;
            $adler = adler32($adler, $str);
            return $adler;
        }

//functions
        function adler32($adler, $str) {
            $BASE = 65521;
            $s1 = $adler & 0xffff;
            $s2 = ($adler >> 16) & 0xffff;
            for ($i = 0; $i < strlen($str); $i++) {
                $s1 = ($s1 + Ord($str[$i])) % $BASE;
                $s2 = ($s2 + $s1) % $BASE;
            }
            return leftshift($s2, 16) + $s1;
        }

//leftshift function
        function leftshift($str, $num) {
            $str = DecBin($str);
            for ($i = 0; $i < (64 - strlen($str)); $i++)
                $str = "0" . $str;
            for ($i = 0; $i < $num; $i++) {
                $str = $str . "0";
                $str = substr($str, 1);
            }
            return cdec($str);
        }

//cdec function
        function cdec($num) {
            $dec = "";
            for ($n = 0; $n < strlen($num); $n++) {
                $temp = $num[$n];
                $dec = $dec + $temp * pow(2, strlen($num) - $n - 1);
            }
            return $dec;
        }
        ?>

        <!-- MY ACCOUNT -->
        <div class="account-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- HTML -->
                        <div id="account-id">
                            <h4 class="account-title"><span class="fa fa-chevron-right"></span>You have participated in this एक Box sale. You can purchase your EkBox for Rs. 599</h4>                                                                    
                            <div class="order-history">
                                <table class="cart-table">
                                    <tr>                                                
                                        <th>Image</th> 
                                        <th>Product</th>
                                        <th>Description</th>  
                                        <th>Status</th>  
                                        <th>total</th>

                                        <th>Offer Price</th>
                                        <th></th>
                                    </tr>
                                    <tr>                                                                    
                                        <td>Desi Cardboard Version 2.0</td>
                                        <td>
                                            <h4>Desi Cardboard Version 2.0 is the latest and improved cardboard. Pre Order today. Starts shipping today</h4>

                                        </td>
                                        <td>Not Used</td>
                                        <td>
                                            Rs999.00</div>
                                        </td> 

                                        <td><div class="item-price">Rs.599.00</div></td>
                                        <td>
                                            <form id="ccavenue" method="post" action="https://www.ccavenue.com/shopzone/cc_details.jsp">
                                                <input type=hidden name="Merchant_Id" value="<?= $Merchant_Id ?>">
                                                <input type="hidden" name="Amount" value="<?= $Amount ?>">
                                                <input type="hidden" name="Order_Id" value="<?= $Order_Id ?>">
                                                <input type="hidden" name="Redirect_Url" value="<?= $Redirect_Url ?>">
                                                <input type="hidden" name="TxnType" value="A">
                                                <input type="hidden" name="ActionID" value="TXN">
                                                <input type="hidden" name="Checksum" value="<?php echo $Checksum; ?>">
                                                <input type="hidden" name="billing_cust_name" value="Niranjan">
                                                <input type="hidden" name="billing_cust_address" value="Anjanapura">
                                                <input type="hidden" name="billing_cust_country" value="India">
                                                <input type="hidden" name="billing_cust_state" value="Karnataka">
                                                <input type="hidden" name="billing_cust_city" value="Bangalore">
                                                <input type="hidden" name="billing_zip" value="560062">
                                                <input type="hidden" name="billing_cust_tel" value="9036144341">
                                                <input type="hidden" name="billing_cust_email" value="niranjan.raju007@gmail.com">
                                                <input type="hidden" name="delivery_cust_name" value="Niranjan">
                                                <input type="hidden" name="delivery_cust_address" value="Anjanapura">
                                                <input type="hidden" name="delivery_cust_country" value="India">
                                                <input type="hidden" name="delivery_cust_state" value="Karnataka">
                                                <input type="hidden" name="delivery_cust_tel" value="9036144341">
                                                <input type="hidden" name="delivery_cust_notes" value="This is for product purchase">
                                                <input type="hidden" name="Merchant_Param" value="">
                                                <input type="hidden" name="billing_zip_code" value="560062">
                                                <input type="hidden" name="delivery_cust_city" value="bangalore">
                                                <input class="btn-black" type="submit" value="Buy Now" />
                                            </form>

                                        </td>
                                    </tr> 

                                </table>

                                <div class="table-btn">
                                </div>
                            </div>                          
                        </div>
                    </div>                        
                </div>
            </div>
        </div>
        <div class="clearfix space20"></div>

        
        