<?php $this->load->view('header') ?>
<body>

    <!-- PRELOADER -->
    <div id="loader"></div>

    <div class="body">
        <!-- TOPBAR -->
        <?php $this->load->view("top-bar") ?>

        <!-- HEADER -->
        <?php $this->load->view("nav-header") ?>

        <style>
            .error-message p{
                color: #F00;
            }
        </style>
        <!-- BREADCRUMBS -->
        <div class="bcrumbs">
            <div class="container">
                <ul>
                    <li><a href="<?= base_url("desicardboard") ?>">Home</a></li>
                    <li>Invite</li>
                </ul>
            </div>
        </div>


        <!-- MY ACCOUNT -->
        <div class="account-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-8">
                        <img src="<?= base_url('assets/images/invite-friends.jpg') ?>">
                        <!-- HTML -->
                        <div class="invitebox">
                            <h3 class="heading1" style="
                                margin-top: 39px;
                                font-size: 46px;
                                ">Invite Your Friends</h3>

                            <form class="ng-pristine ng-valid" method="post" action="<?= base_url("invite-friends")?>">
                                <?php if($this->session->flashdata("success")) { ?>
                                <div class="alert alert-success">
                                    <strong>Success !</strong> <?= $this->session->flashdata("success") ?>
                                </div>
                                <?php } ?>
                                <?php if($this->session->flashdata("error")) { ?>
                                <div class="alert alert-danger">
                                    <strong>Error !</strong> <?= $this->session->flashdata("error") ?>
                                </div>
                                <?php } ?>
                                <div class="control-group">
                                    <div class="controls">
                                        <label>To :</label><br/> 
                                        <input class="text-field" data-error-style="inline" id="recipeintList" name="name" placeholder="Enter a friend's name" value="<?= set_value("name") ?>" type="text">
                                        <span class="error-message"><?= form_error("name") ?></span>
                                    </div>
                                    <div class="controls">
                                        <label>Email :</label><br/> 
                                        <input class="text-field" data-error-style="inline" id="recipeintList" name="email" placeholder="Enter a friend's email address" value="<?= set_value("email") ?>" type="text">
                                        <span class="error-message"><?= form_error("email") ?></span>
                                    </div>
                                </div><br/>

                                <input class="go" id="sendInvite" type="submit" value="Send Invite">&nbsp;</form>


                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="clearfix space20"></div>



<?php $this->load->view('footer') ?>