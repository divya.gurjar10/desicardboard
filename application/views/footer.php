 
           

            <!-- FOOTER COPYRIGHT -->
          <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                          
                            <br>
                            <p>Copyright © 2015 &middot; Desicardboard. All rights reserved</p>
                        </div>
                        <div class="col-md-5">
                            <img src="<?= base_url('assets/images/basic/payment.jpg') ?>" class="pull-right img-responsive payment" alt=""/>
                        </div>
                    </div>
                </div>
            </div>	

        </div>

               <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="ps-slider">
                                <div class="ps-img1">
                                    <img src="<?= base_url('assets/images/products/single/1.jpg') ?>" alt="">
                                </div>
                                <div class="ps-img2">
                                    <img src="<?= base_url('assets/images/products/single/2.jpg') ?>" alt="">
                                </div>
                                <div class="ps-img3">
                                    <img src="<?= base_url('assets/images/products/single/3.jpg') ?>" alt="">
                                </div>
                                <div class="ps-img4">
                                    <img src="<?= base_url('assets/images/products/single/4.jpg') ?>" alt="">
                                </div>
                            </div>
                            <div class="ps-slider-nav">
                                <ul>
                                    <li id="ps-img1"><img src="images/products/single/1.jpg" class="img-responsive" alt=""></li>
                                    <li id="ps-img2"><img src="images/products/single/2.jpg" class="img-responsive" alt=""></li>
                                    <li id="ps-img3"><img src="images/products/single/3.jpg" class="img-responsive" alt=""></li>
                                    <li id="ps-img4"><img src="images/products/single/4.jpg" class="img-responsive" alt=""></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="product-single">
                                <div class="ps-header">
                                    <span class="badge offer">-50%</span>
                                    <h3>Product fashion</h3>
                                    <div class="ratings-wrap">
                                        <div class="ratings">
                                            <span class="act fa fa-star"></span>
                                            <span class="act fa fa-star"></span>
                                            <span class="act fa fa-star"></span>
                                            <span class="act fa fa-star"></span>
                                            <span class="act fa fa-star"></span>
                                        </div>
                                        <em>(6 reviews)</em>
                                    </div>
                                    <div class="ps-price"><span>$ 200.00</span> $ 99.00</div>
                                </div>

                                <div class="ps-stock">
                                    Available: <a href="#">In Stock</a>
                                </div>
                                <div class="sep"></div>
                                <div class="ps-color">
                                    <p>Color<span>*</span></p>
                                    <a class="black" href="#" onclick="return false;"></a>
                                    <a class="red" href="#" onclick="return false;"></a>
                                    <a class="yellow" href="#" onclick="return false;"></a>
                                    <a class="darkgrey" href="#" onclick="return false;"></a>
                                    <a class="litebrown" href="#" onclick="return false;"></a>
                                </div>
                                <div class="space10"></div>
                                <div class="row select-wraps">
                                    <div class="col-md-7">
                                        <p>Size<span>*</span></p>
                                        <select>
                                            <option>XL</option>
                                            <option>XXL</option>
                                            <option>XXXL</option>
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <p>Quantity<span>*</span></p>
                                        <select>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="space20"></div>
                                <div class="share">
                                    <span>
                                        <a href="#" class="fa fa-heart-o" onclick="return false;"></a>
                                        <a href="#" class="fa fa-signal" onclick="return false;"></a>
                                        <a href="#" class="fa fa-envelope-o" onclick="return false;"></a>
                                    </span>
                                    <div class="addthis_native_toolbox"></div>
                                </div>
                                <div class="space20"></div>
                                <div class="sep"></div>
                                <a class="btn-color" href="#">Add to Bag</a>
                                <a class="btn-black" href="#">Go to Details</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="backtotop"><i class="fa fa-chevron-up"></i></div>

        <!-- STYLE SWITCHER 
        ============================================= -->
        <div class="b-settings-panel">
            <div class="settings-section">
                <span>Boxed</span>
                <div class="b-switch">
                    <div class="switch-handle"></div>
                </div>
                <span>Wide</span>
            </div>

            <hr class="dashed" style="margin: 15px 0px;">

            <h5>Main Background</h5>
            <div class="settings-section bg-list">
                <div class="bg-wood_pattern"></div>
                <div class="bg-shattered"></div>
                <div class="bg-vichy"></div>
                <div class="bg-random-grey-variations"></div>
                <div class="bg-irongrip "></div>
                <div class="bg-gplaypattern"></div>
                <div class="bg-diamond_upholstery"></div>
                <div class="bg-denim"></div>
                <div class="bg-crissXcross"></div>
                <div class="bg-climpek"></div>
            </div>

            <hr class="dashed" style="margin: 15px 0px;">

            <h5>Color Scheme</h5>
            <div class="settings-section color-list">
                <div data-src="<?= base_url('css/color-scheme/grass-green.css') ?>" style="background: #64be33"></div>
                <div data-src="<?= base_url('css/color-scheme/green.css') ?>" style="background: #2bba57"></div>
                <div data-src="<?= base_url('css/color-scheme/turquoise.css') ?>" style="background: #2eafbb"></div>
                <div data-src="<?= base_url('css/color-scheme/blue.css') ?>" style="background: #5489de"></div>
                <div data-src="<?= base_url('css/color-scheme/klein-blue.css') ?>" style="background: #4874cd"></div>
                <div data-src="<?= base_url('css/color-scheme/purple.css') ?>" style="background: #7e47da"></div>
                <div data-src="<?= base_url('css/color-scheme/pink.css') ?>" style="background: #ea5192"></div>
                <div data-src="<?= base_url('css/color-scheme/red.css') ?>" style="background: #e34735"></div>
                <div data-src="<?= base_url('css/color-scheme/orange.css') ?>" style="background: #ff6029"></div>
            </div>

            <a href="#" data-src="<?= base_url('assets/css/style.css') ?>" class="reset"><span class="bg-wood_pattern">Reset</span></a>

          
        </div>
        <!-- END STYLE SWITCHER 
        ============================================= -->

		<!-- Javascript -->
		<script src="<?= base_url('assets/js/jquery.js') ?>"></script>
	
		<!-- ADDTHIS -->
		<!--<script type="text/javascript" src="../../../s7.addthis.com/js/300/addthis_widget.js#pubid=ra-557a95e76b3e51d9" async="async"></script>-->
		<script type="text/javascript">
			// Call this function once the rest of the document is loaded
			function loadAddThis() {
				addthis.init()
			}
		</script>
		<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
		<script src="<?= base_url('assets/js/bs-navbar.js') ?>"></script>
		<script src="<?= base_url('assets/js/vendors/isotope/isotope.pkgd.js') ?>"></script>
		<script src="<?= base_url('assets/js/vendors/slick/slick.min.js') ?>"></script>
		<script src="<?= base_url('assets/js/vendors/tweets/tweecool.min.js') ?>"></script>
		<script src="<?= base_url('assets/js/vendors/rs-plugin/js/jquery.themepunch.revolution.min.js') ?>"></script>
		<script src="<?= base_url('assets/js/vendors/rs-plugin/js/jquery.themepunch.tools.min.js') ?>"></script>
		<script src="<?= base_url('assets/js/jquery.sticky.js') ?>"></script>
		<script src="<?= base_url('assets/js/jquery.subscribe-better.js') ?>"></script>
		<!--<script src="../../../code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>-->
		<script src="<?= base_url('assets/js/vendors/select/jquery.selectBoxIt.min.js') ?>"></script>
		<script src="<?= base_url('assets/js/main.js') ?>"></script>

    </body>


</html>