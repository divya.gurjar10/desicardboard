<?php $this->load->view('header') ?>
<body>
    <style>
        .error-message p{
            color: #f00;
        }
    </style>
    <!-- PRELOADER -->
    <div id="loader"></div>

    <div class="body">
        <!-- TOPBAR -->
        <?php $this->load->view("top-bar") ?>

        <!-- HEADER -->
        <?php $this->load->view("nav-header") ?>


        <!-- BREADCRUMBS -->
        <div class="bcrumbs">
            <div class="container">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li>My Account</li>
                </ul>
            </div>
        </div>


        <!-- MY ACCOUNT -->
        <div class="account-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-8">
                        <!-- HTML -->
                        <div id="account-id">
                        <?php if ($this->session->flashdata("error")) { ?>
                                <div class="alert alert-block alert-danger fade in">
                                    <strong>Error !</strong> <?= $this->session->flashdata("error") ?>                
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata("success")) { ?>
                                <div class="alert alert-block alert-success fade in">
                                    <strong>Success !</strong> <?= $this->session->flashdata("success") ?>                
                                </div>
                            <?php } ?>
                            <h4 class="account-title"><span class="fa fa-chevron-right"></span>Change Your Password</h4>                                                                 
                            <div class="account-form">
                                <form id="shipping-zip-form" method="post" action="<?= base_url("change-password") ?>">                                      
                                    <ul class="form-list row">
                                        <li class="col-md-6 col-sm-6">
                                            <label >Password <em>*</em></label>
                                            <input name="pass" type="password" class="input-text">
                                            <span class="error-message"><?= form_error("pass") ?></span>
                                        </li>
                                        <li class="col-md-6 col-sm-6">
                                            <label> Password Confirm <em>*</em></label>
                                            <input name="conf" type="password" class="input-text">
                                            <span class="error-message"><?= form_error("conf") ?></span>
                                        </li>                                               
                                    </ul>
                                    <div class="buttons-set">
                                        <button class="btn-black" type="submit"><span><span>Update</span></span></button>
                                    </div>
                                </form>
                            </div>                                 
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="clearfix space20"></div>


    </div>
    <?php $this->load->view("footer") ?>