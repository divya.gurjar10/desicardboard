<?php $this->load->view("header") ?>
<body>

    <!-- PRELOADER -->
    <div id="loader"></div>

    <div class="body">
        <!-- TOPBAR -->
        <div class="top_bar">
            <div class="container">
                <div class="row">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tb_left pull-left">
                                <p>Join the VR revolution!</p>
                            </div>
                            <div class="tb_center pull-left">
                                <ul>
                                    <li><i class="fa fa-phone"></i> Hotline: <a href="#">+91-9538111016</a></li>
                                    <li><i class="fa fa-envelope-o"></i> <a href="#">support@desicardboard.com</a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- HEADER -->
        <header>
            <nav class="navbar navbar-default">
                <div class="container">
                    <div class="row">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <!-- Logo -->
                            <a class="navbar-brand" href="<?= base_url() ?>"><img src="<?= base_url('assets/images/basic/logo.png') ?>" class="img-responsive" alt=""/></a>
                        </div>

                        <!-- Navmenu -->

                    </div>
                </div>
            </nav>
        </header>

        <!-- BREADCRUMBS -->
        <div class="bcrumbs">
            <div class="container">
                <ul>

                    <li>Forgot Password</li>
                </ul>
            </div>
        </div>
        <div class="space10"></div>

        <!-- MY ACCOUNT -->
        <div class="account-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-6">
                        <!-- HTML -->
                        <div id="account-id">
                            <h4 class="account-title"><span class="fa fa-chevron-right"></span>Forgot Password</h4>                                                                  
                            <div class="account-form">
                                <form class="form-login" action="<?= base_url("forgot-password") ?>" method="post">  
                                    <?php if ($this->session->flashdata("error")) { ?>
                                        <div class="alert alert-block alert-danger fade in">
                                            <strong>Error !</strong> <?= $this->session->flashdata("error") ?>                
                                        </div>
                                    <?php } ?>
                                    <?php if ($this->session->flashdata("success")) { ?>
                                        <div class="alert alert-block alert-success fade in">
                                            <strong>Success !</strong> <?= $this->session->flashdata("success") ?>                
                                        </div>
                                    <?php } ?>
                                    <ul class="form-list row">

                                        <li class="col-md-12 col-sm-12">
                                            <label >Email ID<em>*</em></label>
                                            <input name="email" type="text" class="input-text">
                                            <span style="color: #f00"><?= form_error("email") ?></span>
                                        </li>
                                        <li class="col-md-6 col-sm-12">                                                

                                            <label > <a href="<?= base_url("login") ?>"> Go to Login </a> </label>
                                        </li>

                                    </ul>
                                    <div class="buttons-set">
                                        <button class="btn-black" type="submit"><span>Ask For Password</span></button>
                                    </div>
                                </form>
                            </div>                                    
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-6">
                        <!-- HTML -->
                        <div id="account-id2">
                            <h4 class="account-title"><span class="fa fa-chevron-right"></span>Invitation</h4>                                                                  
                            <div class="account-form create-new-account">
                                <h3 class="block-title">Signup Today using your invitation and You'll be able to</h3>
                                <ul>
                                    <li> <i class="fa fa-edit"></i> Get offer price on Version 2.0</li>
                                    <li> <i class="fa fa-edit"></i> Invite maximum number of friends</li>
                                    <li> <i class="fa fa-edit"></i> Get refund when two of your invitees purchases the product</li>

                                </ul>

                            </div>                                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix space20"></div>




        <?php $this->load->view("footer") ?>