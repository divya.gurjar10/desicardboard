<div class="top_bar">
    <div class="container">
        <div class="row">
            <div class="row">
                <div class="col-md-12">
                    <div class="tb_left pull-left">
                        <p>Join the VR revolution!</p>
                    </div>
                    <div class="tb_center pull-left">
                        <ul>
                            <li><i class="fa fa-phone"></i> Hotline: <a href="#">+91-9538111016</a></li>
                            <li><i class="fa fa-envelope-o"></i> <a href="#">support@desicardboard.com</a></li>
                        </ul>
                    </div>
                    <div class="tb_right pull-right">
                        <ul>
                            <li>
                                <div class="tbr-info">
                                    <span>Account <i class="fa fa-caret-down"></i></span>
                                    <div class="tbr-inner">
                                        <a href="<?= base_url("change-password") ?>">Change Password</a>
                                        <a href="<?= base_url("login/logout") ?>">Logout</a>
                                    </div>
                                </div>
                            </li>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>