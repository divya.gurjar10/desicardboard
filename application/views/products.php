<?php $this->load->view("header") ?>
<body>
    <style>


        .tabbable{
            width: 100%;
            padding-left: 13px;
            padding-right: 15px;
        }
        .tabs-below > .nav-tabs,
        .tabs-right > .nav-tabs,
        .tabs-left > .nav-tabs {
            border-bottom: 0;
        }

        .tab-content > .tab-pane,
        .pill-content > .pill-pane {
            display: none;
        }

        .tab-content > .active,
        .pill-content > .active {
            display: block;
        }

        .tabs-below > .nav-tabs {
            border-top: 1px solid #ddd;
        }

        .tabs-below > .nav-tabs > li {
            /*margin-top: -1px;*/
            margin-bottom: 0;
        }

        .tabs-below > .nav-tabs > li > a {
            -webkit-border-radius: 0 0 4px 4px;
            -moz-border-radius: 0 0 4px 4px;
            border-radius: 0 0 4px 4px;
        }

        .tabs-below > .nav-tabs > li > a:hover,
        .tabs-below > .nav-tabs > li > a:focus {
            border-top-color: #ddd;
            border-bottom-color: transparent;
        }

        .tabs-below > .nav-tabs > .active > a,
        .tabs-below > .nav-tabs > .active > a:hover,
        .tabs-below > .nav-tabs > .active > a:focus {
            border-color: transparent #ddd #ddd #ddd;
        }

        .tabs-left > .nav-tabs > li,
        .tabs-right > .nav-tabs > li {
            float: none;
        }

        .tabs-left > .nav-tabs > li > a,
        .tabs-right > .nav-tabs > li > a {
            min-width: 74px;
            margin-right: 0;
            /*margin-bottom: 3px;*/
        }

        .tabs-left > .nav-tabs {
            float: left;
            margin-right: 19px;
            border-right: 1px solid #ddd;
        }

        .tabs-left > .nav-tabs > li > a {
            margin-right: -1px;
            -webkit-border-radius: 4px 0 0 4px;
            -moz-border-radius: 4px 0 0 4px;
            border-radius: 4px 0 0 4px;
        }

        .tabs-left > .nav-tabs > li > a:hover,
        .tabs-left > .nav-tabs > li > a:focus {
            border-color: #eeeeee #dddddd #eeeeee #eeeeee;
        }

        .tabs-left > .nav-tabs .active > a,
        .tabs-left > .nav-tabs .active > a:hover,
        .tabs-left > .nav-tabs .active > a:focus {
            border-color: #ddd transparent #ddd #ddd;
            *border-right-color: #ffffff;
        }

        .tabs-right > .nav-tabs {
            float: right;
            margin-left: 19px;
            border-left: 1px solid #ddd;
        }

        .tabs-right > .nav-tabs > li > a {
            margin-left: -1px;
            -webkit-border-radius: 0 4px 4px 0;
            -moz-border-radius: 0 4px 4px 0;
            border-radius: 0 4px 4px 0;
        }

        .tabs-right > .nav-tabs > li > a:hover,
        .tabs-right > .nav-tabs > li > a:focus {
            border-color: #eeeeee #eeeeee #eeeeee #dddddd;
        }

        .tabs-right > .nav-tabs .active > a,
        .tabs-right > .nav-tabs .active > a:hover,
        .tabs-right > .nav-tabs .active > a:focus {
            border-color: #ddd #ddd #ddd transparent;
            *border-left-color: #ffffff;
        }

        .side-nav {
            background: #31afe4 none repeat scroll 0 0;

            height: 385px;
            padding-bottom: 10%;
            padding-top: 3%;
        }

        ul.side-nav li.active{
            background: #278cb6 none repeat scroll 0 0;
            border-left: 4px solid #fff;
            color: #FFF;
        }/*
        */
        /*             ul.side-nav li {
                        padding: 10px 15px;
                    }*/

        a.white1{
            color: #FFF;
        }

        .thitry{
            width: 15%;
            float: left
        }

        .seventy{
            width: 85%;
            float: left;
        }
        .nav-stacked > li {
            font-size: 16px;
            height: 36px;
            padding-left: 20px;
            padding-top: 7px;
        }

    </style>
    <!-- PRELOADER -->
    <div id="loader"></div>

    <div class="body">
        <!-- TOPBAR -->
        <?php $this->load->view("top-bar") ?>

        <!-- HEADER -->
        <?php $this->load->view("nav-header") ?>


        <!-- BREADCRUMBS -->
        <div class="bcrumbs">
            <div class="container">
                <ul>
                    <li><a href="<?= base_url("desicardboard") ?>">Home</a></li>
                    <li><a href="#">Offer</a></li>

                </ul>
            </div>
        </div>

        <!-- MY ACCOUNT -->
        <div class="account-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- HTML -->
                        <div id="account-id">

                            <?php if ($this->session->flashdata("error")) { ?>
                                <div class="alert alert-block alert-danger fade in">
                                    <strong>Error !</strong> <?= $this->session->flashdata("error") ?>                
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata("success")) { ?>
                                <div class="alert alert-block alert-success fade in">
                                    <strong>Success !</strong> <?= $this->session->flashdata("success") ?>                
                                </div>
                            <?php } ?>
                            <?php $c = 200-$pro_count ?>
                            <?php if($c > 0 ){?>
                            <!-- <span>
                            <div class="alert alert-info">
                            only <?= $c ?> kits  available for Grab today!
                            </div>
                            </span> -->
                            <?php }  else {?>
                            <div class="alert alert-info">
                            Next Campaign Begins from <?= date("jS M Y 00:01", strtotime("+1 day")); ?>
                            </div>
		            <?php } ?>
                            <h4 class="account-title"><span class="fa fa-chevron-right"></span>YOU JUST GOT LUCKY, GRAB YOUR DEAL BEFORE ITS GONE</h4>                                                                    
                            <div class="order-history">
                                <table class="cart-table">
                                    <tr>                                                
                                        <th>Image</th> 
                                        <th>Product</th>
                                        <th>Description</th>  
                                        <th>Status</th>  
                                        <th>Actual Price</th>

                                        <th>Offer Price</th>
                                        <th></th>
                                    </tr>
                                    <tr>                                              
                                        <td><img src="<?= base_url('assets/images/products/5.jpg') ?>" class="img-responsive" alt=""/></td>                                                                                               
                                        <td>Desi Cardboard Version 2.0</td>
                                        <td>
                                            <h4>Version 2.0 is an improved version with 37mm Wide view lens with a headband which fits phones upto 6 Inches</h4>

                                        </td>
                                        <td>
                                        <?php if($purchased == 0) echo "Offer not availed"; else echo "offer Availed"; ?></td>
                                        <td>
                                            Rs.1111.00</div>
                                        </td> 

                                        <td><div class="item-price">Rs.444.00</div></td>
                                        <td>
                                            <?php if ($available) { ?>
                                                 <form id="ccavenue" method="post" action="<?= base_url("desicardboard") ?>">
    
                                           <!-- Note that the amount is in paise = 50 INR -->
                                            <script
                                            src="https://checkout.razorpay.com/v1/checkout.js"
                                            data-key="rzp_test_7gW8sOMB0OsjJu"
                                            data-name="Desicardboard.com"
                                            data-description="Desi Cardboard with RazorPay"
                                            data-theme.color="#444"
                                            ></script>
                                            <input type="hidden" value="Hidden Element" name="hidden">
                                            <input class="btn-black" type="submit" value="Buy Now" />
                                            <div id="payment_id"  >
                                            <?php
                                            if ($_POST) {
                                                $payment_id = $_POST['razorpay_payment_id'];
                                                echo "Transaction ID: ". $payment_id;
                                            }
                                            
                                            ?>   
                                            </div>
                                            <?php } else { ?>
                                                <?= date("jS M Y 00:01", strtotime("+1 day")); ?>
                                            <?php } ?>

                                        </td>
                                    </tr> 

                                </table>

                                <div class="table-btn" style="margin-bottom: 41px;">
                                    <a href="<?= base_url("invite-friends") ?>" class="btn-black">Generate an Invite</a>
<a href="https://www.youtube.com/watch?v=R4pjjsOP59E" target="_blank">See our short commercial video here</a>
                                </div>
                            </div>                          
                        </div>
                    </div>  



                </div>
            </div>
            
            <div class="container">
            <div class="compactability" style="margin-bottom: 36px;">
   <h4 class="account-title"><span class="fa fa-chevron-right"></span>CHECK YOUR PHONE COMPATIBILITY HERE</h4>                               
  <div class="make" style="
    margin-top: 44px;
"><tr> 
      <td><h4>Select Make<font color="#FF0000" size="4">*</font></h4></td>  
  <td>
         <label>
            <select name="job_location" id="brand">
            <option value="select">Select Brand</option>
            <?php foreach($brand as $row) { ?>
                <option value="<?= $row ?>"><?= $row ?></option>
                <?php } ?>
            </select>
        </label>
   </td>
 </tr></div>
 
 
  <div class="model" style="
    float: left;
    margin-left: 183px;
    margin-top: -65px;
    
">  <tr> 
      <td><h4>Select Model<font color="#FF0000" size="4">*</font></h4></td>  
  <td>
         <label>
            <select name="job_location" placeholder="select date" id="model" >
                        <option value="select">Select Model</option>
               
            </select>
        </label>
   </td>
 </tr></div>
 <div class="clear"></div>
    <span id="text"></span>
<div class="buttonc">
 <a class="btn-black" id="check">Check Compatibility</a>
</div>
                
</div></div>
            <div class="container">
                <h4 class="account-title">Frequently Asked Questions</h4>
                <ul class="nav-stacked thitry side-nav">
                    <li class="active"><a href="#a" class="white1" data-toggle="tab">Product</a></li>
                    <li><a href="#b" class="white1" data-toggle="tab">Order</a></li>
                    <li><a href="#c" class="white1" data-toggle="tab">Payment</a></li>
                    <li><a href="#d" class="white1" data-toggle="tab">Shipping</a></li>
                    <li><a href="#e" class="white1" data-toggle="tab">Apps</a></li>
                    <li><a href="#f" class="white1" data-toggle="tab">Invites</a></li>
                    <li><a href="#g" class="white1" data-toggle="tab">Returns</a></li>
                </ul>
                <div class="tab-content seventy">
                    <div class="tab-pane active" id="a">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">What is Desicardboard VR?</a>
                                    </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse">
                                    <div class="panel-body"><a href="https://www.youtube.com/watch?v=R4pjjsOP59E" target="_blank">Desicardboard</a> VR is a Virtual reality kit inspired by Google cardboard. The kit with your smartphone 
                                        coupled with a VR app will give you an immersive virtual experience of a different world.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">I still don’t get what Virtual reality is all about?</a>
                                    </h4>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse">
                                    <div class="panel-body">Alright! Check this <a href="https://www.youtube.com/watch?v=yRUCU5FsvTY" target="_blank">fairly descriptive video.</a></div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">What is Google cardboard Version 2.0?</a>
                                    </h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body">Google released a Version 1.0 in <a href="https://www.youtube.com/watch?v=DFog2gMnm44" target="_blank">June 2014 IO</a> with a vision to take VR to everyone, However the kit had
                                        few drawbacks. It wouldn’t fit phones above 4.8 Inches and had a magnetic click option which was a 
                                        medium to interact with the phone, Google realized that it restricted the usage of cardboard to be used 
                                        only with few phones, so the new kit was released with a capacitive touch button in <a href="https://www.youtube.com/watch?v=R0nSAYk_IVA" target="_blank">May 2015 Google IO 
                                        conference.</a> The <a href="https://www.youtube.com/watch?v=R4pjjsOP59E" target="_blank">Desicardboard</a> version 2.0 follows the principle design of Google cardboard version 2.0</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">What is the difference between Google cardboard V 2.0 and Desicardboard V2.0?</a>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse">
                                    <div class="panel-body">Google version 2.0 cardboard comes with a Capacitive touch hinge. Whereas <a href="https://www.youtube.com/watch?v=R4pjjsOP59E" target="_blank">Desicardboard</a> V2.0 comes
                                        with a finger access port.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">Why is Desicardboard design different than Google cardboard?</a>
                                    </h4>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        A moving part made out of cardboard, increases the chances of the cardboard tearing depending on the
                                        number of presses and any constant force on it, If in case if the moving part stops functioning the 
                                        cardboard itself becomes useless completely. So keeping that in mind we designed the kit with a finger 
                                        access port which uses our Finger which is a best tool to interact with the screen. So the durability of the 
                                        cardboard is higher, the functionality Simpler and the experience better.
                                    </div>
                                </div>
                            </div>


        <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">Does Desicardboard comes with a headband?</a>
                                    </h4>
                                </div>
                                <div id="collapse6" class="panel-collapse collapse">
                                    <div class="panel-body">
                                       Yes, your <a href="https://www.youtube.com/watch?v=R4pjjsOP59E" target="_blank">Desicardboard</a> comes with a detachable headband which is made of high quality Nylon measuring 36 cms which is a perfect fit & strength for most Indian consumers.
                                    </div>
                                </div>
                            </div>







                        </div> 
                    </div>

                    <div class="tab-pane" id="b">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse11">Do you ship orders outside India?</a>
                                    </h4>
                                </div>
                                <div id="collapse11" class="panel-collapse collapse">
                                    <div class="panel-body">Currently we ship only within India.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse12">I need to change/Update the address on the order. How can I do that?</a>
                                    </h4>
                                </div>
                                <div id="collapse12" class="panel-collapse collapse">
                                    <div class="panel-body">Please write to update@desicardboard.com and we will get the details updated and send you a
                                        confirmation in 24-48 hours. We do not accept changes to be done over the phone and the mail needs 
                                        to be sent from the registered mail ID of your order.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse13">How do I cancel the order?</a>
                                    </h4>
                                </div>
                                <div id="collapse13" class="panel-collapse collapse">
                                    <div class="panel-body">You can cancel your order before it is shipped by sending a mail to update@desicardboard.com and it
                                        would be done within 24 hours.</div>
                                </div>
                            </div>

                        </div> 
                    </div>
                    <div class="tab-pane" id="c">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse21">I am getting an error during payment?</a>
                                    </h4>
                                </div>
                                <div id="collapse21" class="panel-collapse collapse">
                                    <div class="panel-body">Please refresh your cache and try again, if the problem persists again and if the message states
                                        “Transaction declined by the bank” we would recommend to call your bank for assistance on the same.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse22">How secured is my payment with CCavenue?</a>
                                    </h4>
                                </div>
                                <div id="collapse22" class="panel-collapse collapse">
                                    <div class="panel-body">Ccavenue is the most prominent and trusted by customers across india, Ccavenue protects its customers 
                                        payments and has a strong dispute resolution. So you can be rest assured you are completely protected 
                                        on the payments that goes through CCavenue.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse23">Do you offer COD?</a>
                                    </h4>
                                </div>
                                <div id="collapse23" class="panel-collapse collapse">
                                    <div class="panel-body">At this point we do not offer COD option.</div>
                                </div>
                            </div>

                        </div> 
                    </div>
                    <div class="tab-pane" id="d">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse31">In how many days will my order be shipped?</a>
                                    </h4>
                                </div>
                                <div id="collapse31" class="panel-collapse collapse">
                                    <div class="panel-body">We ship the order within 5-10 business days and you can expect to receive the kit within 12-15 working days.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse32">Would I receive a tracking number?</a>
                                    </h4>
                                </div>
                                <div id="collapse32" class="panel-collapse collapse">
                                    <div class="panel-body">We would be sending a mail with the tracking details, so just in case your login to your account you may 
                                        see the status of your order as “in process”. We would request you to check your mail, including Spam 
                                        to stay in line for your order.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse33">Where do you ship from?</a>
                                    </h4>
                                </div>
                                <div id="collapse33" class="panel-collapse collapse">
                                    <div class="panel-body">We ship the order from Bangalore, India</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse34">Do you ship the order through Air or Surface?</a>
                                    </h4>
                                </div>
                                <div id="collapse34" class="panel-collapse collapse">
                                    <div class="panel-body">All our orders are shipped through Air and usually takes about 2-3 days to be delivered</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse35">Who is your courier partner?</a>
                                    </h4>
                                </div>
                                <div id="collapse35" class="panel-collapse collapse">
                                    <div class="panel-body">Currently we use Professional couriers and First Flight courier.</div>
                                </div>
                            </div>

                        </div> 
                    </div>

                    <div class="tab-pane" id="e">
                        <a href="https://play.google.com/store/apps/collection/promotion_3001527_cardboard_apps ">  <img src="http://desicardboard.com/get/assets/images/comingsoonandroid.png"></a>
                        <a href="https://itunes.apple.com/us/app/vrse-virtual-reality/id959327054?mt=8">  <img src="http://desicardboard.com/get/assets/images/comingsoonapple.png"></a>
                    </div>

                    <div class="tab-pane" id="f">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse51">My invite expired; can I get a new one?</a>
                                    </h4>
                                </div>
                                <div id="collapse51" class="panel-collapse collapse">
                                    <div class="panel-body">When an invite expires, the corresponding Invite is allocated to someone else waiting for an invite. 
                                        Unfortunately, we cannot reissue invites every time one expires. To ensure you are seeing all email's 
                                        from <a href="https://www.youtube.com/watch?v=R4pjjsOP59E" target="_blank">Desicardboard</a>, make sure email's from Desicardboard.com don’t end up in your spam or hidden 
                                        under the Promotions tab by adding it to your contacts.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse52">What do I receive in return if I Invite and get friends to buy it?</a>
                                    </h4>
                                </div>
                                <div id="collapse52" class="panel-collapse collapse">
                                    <div class="panel-body">You are our ambassador, If you invite 2 of your friends to buy the Version 2.0, we would completely 
                                        refund your order amount. Yes! Your kit would be absolutely free.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse53">How many invites can I send?</a>
                                    </h4>
                                </div>
                                <div id="collapse53" class="panel-collapse collapse">
                                    <div class="panel-body">Currently you can send 5 Invites to your friends, 7 days from the date of your purchase. So make sure
                                        you use them with care.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse54">I don’t want to buy the kit but want to refer my friend?</a>
                                    </h4>
                                </div>
                                <div id="collapse54" class="panel-collapse collapse">
                                    <div class="panel-body">You can talk about us, however you would not be able to generate an invite without a purchase.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse55">I need an invite?</a>
                                    </h4>
                                </div>
                                <div id="collapse55" class="panel-collapse collapse">
                                    <div class="panel-body">Please submit your mail ID for a request of invite in the main page.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse56">Why do you have limited Kits in this offer?</a>
                                    </h4>
                                </div>
                                <div id="collapse56" class="panel-collapse collapse">
                                    <div class="panel-body">We have limited capabilities to manufacture and depending on the demand we would increase the 
                                        numbers in the coming days.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse57">I referred 3 friends, when can I get the refund?</a>
                                    </h4>
                                </div>
                                <div id="collapse57" class="panel-collapse collapse">
                                    <div class="panel-body">If you have successfully referred 3 friends to buy the kit we would initiate a refund as soon as your 
                                        friends kits are shipped, which would take max 5 business days and the amount would show up in your 
                                        account of purchase within another 5-7 days accordance to your bank process.</div>
                                </div>
                            </div>

                        </div> 
                    </div>
     
<div class="tab-pane" id="g">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse61">I received a damaged product? how do i return it?</a>
                                    </h4>
                                </div>
                                <div id="collapse61" class="panel-collapse collapse">
                                    <div class="panel-body">We ensure that the product is packaged well and cushioned well to handle the usual desi courier handling way and we wrap the lens with a electro static sheet to ensure no scratches, so there is hardly any way you would receive a damaged kit, even if you do manage to get a damaged kit, contact our customer care or write to us and we would ensure you get a perfect virtual reality experience.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse62">I haven't received the kit since a week. Can you reship the same?</a>
                                    </h4>
                                </div>
                                <div id="collapse62" class="panel-collapse collapse">
                                    <div class="panel-body">We ship the kit through reputed couriers and share the tracking number through mail on the same day of shipping. we would request you to follow up with your local courier office using the tracking details. Even if we manage to ship the order on rare cases due to courier you may face some issue but we are here to help and again to ensure you get the best product and experience. Call us or mail us and we are at your support.</div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse63">I want to return the kit and get a refund?</a>
                                    </h4>
                                </div>
                                <div id="collapse63" class="panel-collapse collapse">
                                    <div class="panel-body">Write to us or call us, we would be happy to do whatever it takes to get the best experience possible. </div>
                                </div>
                            </div>

                        </div> 
                    </div>
                </div>
            </div> 
        </div>
        <div class="clearfix space20"></div>


        <?php $this->load->view("footer") ?>
<!-- <script>
        $(document).on("change","#brand",function(){
            var val = $(this).val();
            
            if(val != "" && val != "select"){
                $.ajax({
                    url: "<?= base_url("get-model") ?>",
                    data:{"value":val},
                    type:"POST",
                    dataType: "HTML",
                    success: function(data){
                        $("#model").html(data);
                    }
                });
            }
        });
        
         $(document).on("click","#check",function(){
            var val = $("#model").val();
            var brand = $("#brand").val();            
            
            if(val != "" && val != "select" && brand != "" && brand != "select"){
                $.ajax({
                    url: "<?= base_url("get-text") ?>",
                    data:{"model":val,"brand":brand},
                    type:"POST",
                    dataType: "HTML",
                    success: function(data){
                        $("#text").html(data);
                    }
                });
            }
        });
        
        </script> -->