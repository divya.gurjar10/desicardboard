<header>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Logo -->
                    <a class="navbar-brand" href="<?= base_url("desicardboard") ?>"><img src="<?= base_url('assets/images/basic/logo.png') ?>" class="img-responsive" alt=""/></a>
                </div>

                <!-- Navmenu -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?= base_url("desicardboard") ?>">Home</a>

                        <li><a href="<?= base_url("invite-friends") ?>">Invite Friend</a>
                        <li><a href="<?= base_url("track-invitation") ?>">Track Invitation</a>


                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>