<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->


    <head>

        <!-- Meta -->
        <meta charset="utf-8">
        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Desicardboard VR</title>

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Favicon -->
        <link rel="shortcut icon" href="http://jakjim.com/favicon.ico">

        <!-- Google Webfont -->
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,200,100,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lato:400,100,300,300italic,700,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

        <!-- CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/css/font-awesome/css/font-awesome.css') ?>">
        <!--<link rel="stylesheet" href="css/bootstrap.min.css">-->
        <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.css') ?>">
        <link rel="stylesheet" href="<?= base_url('assets/js/vendors/isotope/isotope.css') ?>">
        <link rel="stylesheet" href="<?= base_url('assets/js/vendors/slick/slick.css') ?>">
        <link rel="stylesheet" href="<?= base_url('assets/js/vendors/rs-plugin/css/settings.css') ?>">
        <link rel="stylesheet" href="<?= base_url('assets/js/vendors/select/jquery.selectBoxIt.css') ?>">
        <link rel="stylesheet" href="<?= base_url('assets/css/subscribe-better.css') ?>">
        <!--<link rel="stylesheet" href="<?= base_url('assets/../../../ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/ui-lightness/jquery-ui.css') ?>">-->
        <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
                <![endif]-->

    </head>
    