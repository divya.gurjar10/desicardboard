<?php $this->load->view("header") ?>
<body>

    <!-- PRELOADER -->
    <div id="loader"></div>

    <div class="body">
        <!-- TOPBAR -->
        <?php $this->load->view("top-bar") ?>

        <!-- HEADER -->
        <?php $this->load->view("nav-header") ?>

        <!-- BREADCRUMBS -->
        <div class="bcrumbs">
            <div class="container">
                <ul>
                    <li><a href="<?= base_url("desicardboard") ?>">Home</a></li>
                    <li><a href="#">Track Invitation</a></li>

                </ul>
            </div>
        </div>


        <!-- MY ACCOUNT -->
        <div class="account-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- HTML -->
                        <div id="account-id">
                            <h4 class="account-title"><span class="fa fa-chevron-right"></span>Track your invitations</h4>                                                                    
                            <div class="order-history">
                                <table class="cart-table">
                                    <tr>                                                
                                        <th>Email Address</th> 
                                        <th>Status</th>
                                        <th>Product Purchase Status</th>
                                        <th>Expiring on</th>  



                                    </tr>

                                    <?php if (count($res)) { ?>
                                        <?php foreach ($res as $row) { ?>
                                            <tr> 
                                                <td><?= $row->email ?></td>                                                                                               
                                                <td>Invited</td>
                                                <td><?php if(!$row->purchased){ echo "Not yet purchased"; } else { echo "Purchased"; } ?></td>
                                                <!--date('Y-m-d', strtotime($Date. ' + 1 days'));-->
                                                <td><?= date("d-m-Y | h:i:s A | l, M",strtotime($row->date_added . "+2 day")) ?></td> 
                                            </tr>

                                        <?php } } else { ?>
                                        <tr><td colspan="4" align="center">No Invitations Sent</td></tr>
                                    <?php } ?>


                                </table>

                                <div class="table-btn">
                                    <a href="<?= base_url("invite-friends") ?>" class="btn-black">Generate more Invite's</a>
                                </div>
                            </div>                          
                        </div>
                    </div>                        
                </div>
            </div>
        </div>
        <div class="clearfix space20"></div>



<?php $this->load->view("footer") ?>