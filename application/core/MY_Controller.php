<?php

class Admin_Controller extends CI_Controller {

    function __construct() 
    {
        parent::__construct();
        $this->no_cache();
        if ($this->session->userdata('id')) 
        {
            redirect('login');
        }
    }

    Public function no_cache() 
    {
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i(worry)") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    }

    function pre($data) 
    {
        echo '<pre>';
        print_r($data);
        echo "</pre>";
        exit;
    }

}
