<?php $this->load->view("header") ?>
    <body>

        <!-- PRELOADER -->
        <div id="loader"></div>

        <div class="body">
            <!-- TOPBAR -->
            <?php $this->load->view("top-bar") ?>

            <!-- HEADER -->
            <?php $this->load->view("nav-header") ?>


            <!-- BREADCRUMBS -->
            <div class="bcrumbs">
                <div class="container">
                    <ul>
                        <li><a href="<?= base_url("desicardboard") ?>">Home</a></li>
                        <li><a href="#">Offer</a></li>
                      
                    </ul>
                </div>
            </div>


            <!-- MY ACCOUNT -->
            <div class="account-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- HTML -->
                            <div id="account-id">
                                <h4 class="account-title"><span class="fa fa-chevron-right"></span>You have participated in this एक Box sale. You can purchase your EkBox for Rs. 599</h4>                                                                    
                                <div class="order-history">
                                    <table class="cart-table">
                                        <tr>                                                
                                            <th>Image</th> 
                                            <th>Product</th>
                                            <th>Description</th>  
                                            <th>Status</th>  
                                            <th>total</th>
                                            
                                            <th>Offer Price</th>
                                            <th></th>
                                        </tr>
                                        <tr>                                              
                                            <td><img src="<?= base_url('assets/images/products/5.jpg') ?>" class="img-responsive" alt=""/></td>                                                                                               
                                            <td>Desi Cardboard Version 2.0</td>
                                            <td>
                                                <h4>Desi Cardboard Version 2.0 is the latest and improved cardboard. Pre Order today. Starts shipping today</h4>
                                               
                                            </td>
                                            <td>Not Used</td>
                                            <td>
                                                Rs999.00</div>
                                            </td> 
                                            
                                            <td><div class="item-price">Rs.599.00</div></td>
                                            <td>
                                                <a href="#" class="btn-black">Buy Now</a>
                                               
                                            </td>
                                        </tr> 
                                       
                                    </table>

                                    <div class="table-btn">
                                        <a href="<?= base_url("invite-friends") ?>" class="btn-black">Generate an Invite</a>
                                    </div>
                                </div>                          
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="clearfix space20"></div>


<?php $this->load->view("footer") ?>