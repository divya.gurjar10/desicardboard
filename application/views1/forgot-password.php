<?php $this->load->view("header") ?>
<body>

    <!-- PRELOADER -->
    <div id="loader"></div>

    <div class="body">
        <!-- TOPBAR -->
        <?php $this->load->view("top-bar") ?>

        <!-- HEADER -->
        <?php $this->load->view("nav-header") ?>

        <!-- BREADCRUMBS -->
        <div class="bcrumbs">
            <div class="container">
                <ul>

                    <li>Forgot Password</li>
                </ul>
            </div>
        </div>
        <div class="space10"></div>

        <!-- MY ACCOUNT -->
        <div class="account-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-6">
                        <!-- HTML -->
                        <div id="account-id">
                            <h4 class="account-title"><span class="fa fa-chevron-right"></span>Forgot Password</h4>                                                                  
                            <div class="account-form">
                                <form class="form-login" action="<?= base_url("forgot-password") ?>" method="post">  
                                    <?php if ($this->session->flashdata("error")) { ?>
                                        <div class="alert alert-block alert-danger fade in">
                                            <strong>Error !</strong> <?= $this->session->flashdata("error") ?>                
                                        </div>
                                    <?php } ?>
                                    <?php if ($this->session->flashdata("success")) { ?>
                                        <div class="alert alert-block alert-success fade in">
                                            <strong>Success !</strong> <?= $this->session->flashdata("success") ?>                
                                        </div>
                                    <?php } ?>
                                    <ul class="form-list row">

                                        <li class="col-md-12 col-sm-12">
                                            <label >Email ID<em>*</em></label>
                                            <input name="email" type="text" class="input-text">
                                            <span style="color: #f00"><?= form_error("email") ?></span>
                                        </li>
                                        <li class="col-md-6 col-sm-12">                                                

                                            <label > <a href="<?= base_url("login") ?>"> Go to Login </a> </label>
                                        </li>

                                    </ul>
                                    <div class="buttons-set">
                                        <button class="btn-black" type="submit"><span>Ask For Password</span></button>
                                    </div>
                                </form>
                            </div>                                    
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-6">
                        <!-- HTML -->
                        <div id="account-id2">
                            <h4 class="account-title"><span class="fa fa-chevron-right"></span>Invitation</h4>                                                                  
                            <div class="account-form create-new-account">
                                <h3 class="block-title">Signup Today using your invitation and You'll be able to</h3>
                                <ul>
                                    <li> <i class="fa fa-edit"></i> Get offer price on Version 2.0</li>
                                    <li> <i class="fa fa-edit"></i> Invite maximum number of friends</li>
                                    <li> <i class="fa fa-edit"></i> Get refund when two of your invitees purchases the product</li>

                                </ul>

                            </div>                                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix space20"></div>




        <?php $this->load->view("footer") ?>